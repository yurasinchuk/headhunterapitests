# About Framework #

Example of test framework for [HeadHunter API](https://github.com/hhru/api). Used technologies and tools: **[Java](https://www.oracle.com/java/index.html), [HttpClient by Apache](http://hc.apache.org/httpcomponents-client-4.5.x/index.html), [JUnit](http://junit.org/), [QATools Properties](https://github.com/qatools/properties),[JSON for Java](https://github.com/FasterXML/jackson), [Maven](https://maven.apache.org/), [Jenkins](http://jenkins-ci.org/)**

Example of user.properties file (src/main/resources):

```
#!java

user.uri=https://api.hh.ru
user.agent=TestFramework/1.0 (yurasinchuk@gmail.com)
user.host=api.hh.ru
user.accept=*/*
user.token=Bearer GGU7H6*************************INP05G478
user.notvalidtoken=Bearer GGU7H602L9*******************INP05G478
```

