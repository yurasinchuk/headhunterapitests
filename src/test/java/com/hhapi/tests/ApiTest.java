package com.hhapi.tests;

import com.hhapi.utils.CustomHttps;
import com.hhapi.utils.DeserializationErrorsUtils;
import com.hhapi.utils.DeserializationUserDataUtils;
import com.hhapi.utils.userConfig;
import org.junit.Test;
import ru.qatools.properties.PropertyLoader;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ApiTest {

    userConfig config = PropertyLoader.newInstance()
            .populate(userConfig.class);

    public String uri = config.getConfURI();
    public String userAgent = config.getConfUserAgent();
    public String host = config.getConfUserHost();
    public String accept = config.getConfUserAccept();
    public String accessToken = config.getConfUserToken();
    public String notValidAccessToken = config.getNotValidToken();

    @Test
    public void getInformationAboutRegisteredUserTest() throws IOException {

        String resourceURI = uri + "/me";

        String response = CustomHttps.httpGet(resourceURI, userAgent, host, accept, accessToken, 200);

        System.out.println("Response from " + resourceURI + ": " + response);

        assertEquals("Юрий", DeserializationUserDataUtils.getUserName(response));

        assertEquals("Синчук", DeserializationUserDataUtils.getUserLastName(response));

    }

    @Test
    public void getInformationAboutRegisteredUserAuthorizationTokenIsNotValidTest() throws IOException {

        String resourceURI = uri + "/me";

        String response = CustomHttps.httpGet(resourceURI, userAgent, host, accept, notValidAccessToken, 403);

        System.out.println("Response from " + resourceURI + ": " + response);

        assertEquals("oauth", DeserializationErrorsUtils.getErrorType(response));

        assertEquals("bad_authorization", DeserializationErrorsUtils.getErrorValue(response));

        assertEquals("Forbidden", DeserializationErrorsUtils.getErrorsDescription(response));

    }

    @Test
    public void getListOfCVsForRegisteredUserTest() {

        String resourceURI = uri + "/resumes/mine";

        String response = CustomHttps.httpGet(resourceURI, userAgent, host, accept, accessToken, 200);

        System.out.println("Response from " + resourceURI + ": " + response);
    }

    @Test
    public void getListOfFavoritesVacanciesForRegisteredUserTest() {

        String resourceURI = uri + "/vacancies/favorited";

        String response = CustomHttps.httpGet(resourceURI, userAgent, host, accept, accessToken, 200);

        System.out.println("Response from " + resourceURI + ": " + response);
    }
}