package com.hhapi.pojo.error;

import java.lang.*;
import java.util.List;

public class ErrorsContainer {

    List<Error> errors;

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ErrorsContainer{" +
                "errors=" + errors +
                '}';
    }
}
