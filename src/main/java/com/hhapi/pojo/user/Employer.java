package com.hhapi.pojo.user;

public class Employer {

    private String id;

    private String manager_id;

    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employer{" +
                "id='" + id + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
