package com.hhapi.pojo.user;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class RegisteredUser {

    @JsonProperty("id")
    private String id;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("is_in_search")
    private Boolean isInSearch;

    @JsonProperty("is_anonymous")
    private Boolean isAnonymous;

    @JsonProperty("resumes_url")
    private String resumesUrl;

    @JsonProperty("is_employer")
    private Boolean isEmployer;

    @JsonProperty("personal_manager")
    private Employer personalManager;

    @JsonProperty("email")
    private String email;

    @JsonProperty("manager")
    private Employer manager;

    @JsonProperty("is_admin")
    private Boolean isAdmin;

    @JsonProperty("is_applicant")
    private Boolean isApplicant;

    @JsonProperty("negotiations_url")
    private String negotiationsUrl;

    @JsonProperty("employer")
    private Employer employer;

    @JsonProperty("mid_name")
    private String midName;

    @JsonIgnore
    @JsonProperty("counters")
    private List<Counters> counters;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        return "RegisteredUser{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", isInSearch=" + isInSearch +
                ", isAnonymous=" + isAnonymous +
                ", resumesUrl='" + resumesUrl + '\'' +
                ", isEmployer=" + isEmployer +
                ", personalManager=" + personalManager +
                ", email='" + email + '\'' +
                ", manager=" + manager +
                ", isAdmin=" + isAdmin +
                ", isApplicant=" + isApplicant +
                ", negotiationsUrl='" + negotiationsUrl + '\'' +
                ", employer=" + employer +
                ", counters=" + counters +
                '}';
    }
}
