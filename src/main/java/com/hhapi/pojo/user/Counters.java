package com.hhapi.pojo.user;

public class Counters {

    private int unread_negotiations;

    private int new_resume_views;

    public int getUnread_negotiations() {
        return unread_negotiations;
    }

    public void setUnread_negotiations(int unread_negotiations) {
        this.unread_negotiations = unread_negotiations;
    }

    @Override
    public String toString() {
        return "Counters{" +
                "unread_negotiations=" + unread_negotiations +
                ", new_resume_views=" + new_resume_views +
                '}';
    }
}
