package com.hhapi.utils;

import ru.qatools.properties.Property;
import ru.qatools.properties.Resource;

@Resource.Classpath("user.properties")
public interface userConfig {

    @Property("user.uri")
    String getConfURI();

    @Property("user.agent")
    String getConfUserAgent();

    @Property("user.host")
    String getConfUserHost();

    @Property("user.accept")
    String getConfUserAccept();

    @Property("user.token")
    String getConfUserToken();

    @Property("user.notvalidtoken")
    String getNotValidToken();

    }

