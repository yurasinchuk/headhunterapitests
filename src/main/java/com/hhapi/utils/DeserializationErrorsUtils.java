package com.hhapi.utils;

import com.hhapi.pojo.error.ErrorsContainer;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class DeserializationErrorsUtils {

    public static String getErrorsDescription(String errors) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        ErrorsContainer restoredResp = mapper.readValue(errors, ErrorsContainer.class);

        System.out.println("Error description: "+ restoredResp.getDescription());

        return restoredResp.getDescription();

    }

    public static String getErrorType(String errors) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        ErrorsContainer restoredResp = mapper.readValue(errors, ErrorsContainer.class);

        System.out.println("Error type: "+ restoredResp.getErrors().get(0).getType());

        return restoredResp.getErrors().get(0).getType();

    }

    public static String getErrorValue(String errors) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        ErrorsContainer restoredResp = mapper.readValue(errors, ErrorsContainer.class);

        System.out.println("Error type: "+ restoredResp.getErrors().get(0).getValue());

        return restoredResp.getErrors().get(0).getValue();

    }
}
