package com.hhapi.utils;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;

import static org.junit.Assert.assertEquals;

public class CustomHttps {

    public static String httpGet(String uri, String headerUserAgent, String headerHost, String headerAccept, String headerAuthorization, int expectedStatusCode) throws RuntimeException {

        StringBuilder result = new StringBuilder();

        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpGet getRequest = new HttpGet(uri);

            if (!headerUserAgent.isEmpty()) {
                getRequest.addHeader(HttpHeaders.USER_AGENT, headerUserAgent);
            }

            if (!headerHost.isEmpty()) {
                getRequest.addHeader(HttpHeaders.HOST, headerHost);
            }

            if (!headerAccept.isEmpty()) {
                getRequest.addHeader(HttpHeaders.ACCEPT, headerAccept);
            }

            if (!headerAuthorization.isEmpty()) {
                getRequest.addHeader(HttpHeaders.AUTHORIZATION, headerAuthorization);
            }
            HttpResponse response = httpClient.execute(getRequest);

            int statusCode = response.getStatusLine().getStatusCode();

            /*
            if (statusCode != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("ERROR: HTTP code: " + statusCode);
            }
            */

            assertEquals(expectedStatusCode, statusCode);

            System.out.println("Status code: " +statusCode + "\nExpected status code: " + expectedStatusCode);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = "";
            while((line = br.readLine()) != null){
                result.append(line);
            }
            br.close();

        } catch (ConnectException e) {
            System.err.println("ConnectException: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpClient.getConnectionManager().shutdown();
        }

        return result.toString();
    }
}
