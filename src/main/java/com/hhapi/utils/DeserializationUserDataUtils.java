package com.hhapi.utils;

import com.hhapi.pojo.user.RegisteredUser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class DeserializationUserDataUtils {

    public static String getUserName(String response) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        RegisteredUser regUser = mapper.readValue(response, RegisteredUser.class);

        System.out.println("User first name: " + regUser.getFirstName());

        return regUser.getFirstName();

    }

    public static String getUserLastName(String response) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        RegisteredUser regUser = mapper.readValue(response, RegisteredUser.class);

        System.out.println("User last name: " + regUser.getLastName());

        return regUser.getLastName();

    }
}
